# The title of the project

A small description about the project.

## Contribution

Thank you for your interest in our project. There are many ways to contribute,
and we appreciate all of them :

- Source code
- Unit tests
- Bug Reports
- Documentation
- Localization (Translation)
- etc

Contributions should be under the terms of the MIT license [&lt;LICENSE&gt;](LICENSE).

## How to build

> See [BUILD.md](BUILD.md)

## Authors / Contributors

The project was originally developed by:

- [Amine Ben Hassouna](https://github.com/aminosbh)
- The others here

## License

This project is licensed under the [MIT](LICENSE) license.
